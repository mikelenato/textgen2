import tarfile
import os
import sys
import gpt_2_simple as gpt2
import mtranslate
from flask import Flask, request, jsonify

# Init app
app = Flask(__name__)

def copy_checkpoint_from_local(tar_file):
    """Copies the checkpoint folder from a local FS."""
    file_path = "./" + tar_file
    print("sto scompattando il tar...")
    with tarfile.open(file_path, 'r') as tar:
        tar.extractall()

@app.route("/")
def hello():
    return "Benvenuto! Scrivi le caratteristiche del capo di abbigliamento nella barra del browser..."

@app.route('/api', methods=['GET']) #http://mysite.com/api/
def esegui():

    # pt = translator.translate(p, dest='en').text   #decommentare se p è in italiano
    p = request.args.get('SEED', type=str)
    g = mtranslate.translate(p, 'en')
    #print('sto generando il testo...')

    text = gpt2.generate(sess,
                            run_name='run1',
                            length=70,
                            temperature=0.5,
                            top_k=40,
                            top_p=0.9,
                            prefix=g,
                            nsamples=1,
                            batch_size=1,
                            truncate='<|endoftext|>',
                            include_prefix=False,
                            return_as_list=True
                            )[0]

    try:
        return jsonify(mtranslate.translate(text, 'it')), 200 #sostituire g con text

    except:
        return jsonify('ERRORE DELLA LIBRERIA DI TRADUZIONE'), 500


#if __name__ == "__main__" :
sess = gpt2.start_tf_sess()
if not os.path.exists('checkpoint'):
    print('caricamento del modello nel sistema...')
    copy_checkpoint_from_local(tar_file=os.getenv("MODEL")) #copia dal docker host al container
    print("modello caricato nel sistema!")
print("caricamento del modello in memoria...")
gpt2.load_gpt2(sess, run_name='run1')  # carica il modello dal volume del container alla RAM
print('modello caricato in memoria!')

port = int(os.getenv("PORT", 5000))
host = os.getenv("HOST", '0.0.0.0')
app.run(host=host, port=port)