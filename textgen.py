import tarfile
import os
import sys
import gpt_2_simple as gpt2
import mtranslate
import warnings



def copy_checkpoint_from_local(tar_file):
    """Copies the checkpoint folder from a local FS."""
    file_path = "./" + tar_file
    print("sto scompattando il tar...")
    with tarfile.open(file_path, 'r') as tar:
        tar.extractall()


def esegui(p):

    # pt = translator.translate(p, dest='en').text   #decommentare se p è in italiano

    g = mtranslate.translate(p, 'en')
    #print('sto generando il testo...')
    text = gpt2.generate(sess,
                            run_name='run1',
                            length=100,
                            temperature=0.5,
                            top_k=40,
                            top_p=0.9,
                            prefix=g,
                            nsamples=1,
                            batch_size=1,
                            truncate='<|endoftext|>',
                            include_prefix=False,
                            return_as_list=True
                            )[0]

    try:
        # print('descrizione n. ', + i + 'scritta')
        return mtranslate.translate(text, 'it')
        #print('file con le descrizioni salvato')
    except:
        pass

if __name__ == "__main__" :
    warnings.filterwarnings("ignore")
    sess = gpt2.start_tf_sess()
    if not os.path.exists('checkpoint'):
        print('caricamento del modello nel sistema...')
        copy_checkpoint_from_local(tar_file=os.getenv("MODEL")) #copia dal docker host al container
        print("modello caricato nel sistema!")
        #copy_checkpoint_from_local(tar_file="checkpoint_run1.tar")
    print("caricamento del modello in memoria...")
    gpt2.load_gpt2(sess, run_name='run1')  # carica il modello dal volume del container alla RAM
    print('modello caricato in memoria!')

    #p = input('scrivi alcune caratteristiche del capo di abbigliamento ("e" per uscire):')
    #p = sys.stdin(readline())
    #if p == 'e':
    #    sys.exit()
    print('sto generando la descrizione...')
    with open(os.getenv("INPUT"), 'r') as f:
        for line in f:
            text = esegui(p=line)
            with open(os.getenv("RESULT"), 'a') as fw:
                fw.write(text + '\n')
        print('file con le descrizioni salvato')

    #n = input('scrivi quante descrizioni vuoi:')