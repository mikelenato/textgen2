#FROM tensorflow/tensorflow:latest-devel-gpu
FROM  tensorflow/tensorflow:latest-devel
#COPY start.py start.py
RUN mkdir -p /opt/mymodels/
WORKDIR /opt/mymodels/
RUN python3 -m pip install --upgrade pip
#RUN pip install requirements.txt
RUN pip install tensorflow==1.15
#RUN pip install tensorflow-gpu==1.15  # GPU
RUN pip install mtranslate
RUN pip install -q gpt-2-simple
RUN pip install flask
#RUN cd opt/mymodels/
CMD python textgen.py
#COPY INPUT.txt /opt/mymodels/
#CMD python start.py