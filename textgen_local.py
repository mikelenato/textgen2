import os
import sys
import gpt_2_simple as gpt2
import mtranslate


if __name__ == "__main__" :
    sess = gpt2.start_tf_sess()
    #copy_checkpoint_from_local(tar_file="checkpoint_run1.tar")
    #gpt2.load_gpt2(sess, run_name='run1')  # carica dal volume del container alla RAM
    p = input('scrivi alcune caratteristiche del capo di abbigliamento:')
    g = mtranslate.translate(p, 'en')
    #print('sto generando il testo...')
    text = gpt2.generate(sess,
                            run_name='run1',
                            length=70,
                            temperature=0.5,
                            top_k=40,
                            top_p=0.9,
                            prefix=g,
                            nsamples=1,
                            batch_size=1,
                            truncate='<|endoftext|>',
                            include_prefix=False,
                            return_as_list=True
                            )[0]


    with open('descrizioni.txt', 'a') as f:
        f.write(text + '\n')
    print('file con le descrizioni salvato')